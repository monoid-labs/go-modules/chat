package chat

import (
	"testing"
	"time"
)

func TestLoginCodec(t *testing.T) {
	cmd := login{
		User: "john-1234-doe",
	}
	res := login{}

	var buffer [0xFFFF]byte
	var err error

	err = cmd.Encode(buffer[:cmd.Size()])
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	err = res.Decode(buffer[:cmd.Size()])
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	if cmd.User != res.User {
		t.Logf("User Missmath: expected='%v' actual='%v'\n", cmd.User, res.User)
		t.Fail()
	}
}

func TestJoinCodec(t *testing.T) {
	cmd := join{
		Channel: "john-1234-doe",
	}
	res := join{}

	var buffer [0xFFFF]byte
	var err error

	err = cmd.Encode(buffer[:cmd.Size()])
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	err = res.Decode(buffer[:cmd.Size()])
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	if cmd.Channel != res.Channel {
		t.Logf("Channel Missmath: expected='%v' actual='%v'\n", cmd.Channel, res.Channel)
		t.Fail()
	}
}

func TestReceiveCodec(t *testing.T) {
	cmd := receive{
		Time:    time.Now(),
		User:    "john-1234-doe",
		Message: "hello\nanother world!",
		Channel: "rand-777",
	}
	res := receive{}

	var buffer [0xFFFF]byte
	var err error

	err = cmd.Encode(buffer[:cmd.Size()])
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	err = res.Decode(buffer[:cmd.Size()])
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	if cmd.User != res.User {
		t.Logf("Channel Missmath: expected='%v' actual='%v'\n", cmd.User, res.User)
		t.Fail()
	}
	if cmd.Message != res.Message {
		t.Logf("Channel Missmath: expected='%v' actual='%v'\n", cmd.Message, res.Message)
		t.Fail()
	}
	if cmd.Channel != res.Channel {
		t.Logf("Channel Missmath: expected='%v' actual='%v'\n", cmd.Channel, res.Channel)
		t.Fail()
	}
}
