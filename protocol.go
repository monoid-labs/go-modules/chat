package chat

import (
	"encoding/binary"
	"fmt"
	"time"
)

type cmdType byte

const (
	cmdLogin cmdType = iota
	cmdJoin
	cmdLeave
	cmdSend
	cmdReceive
)

type cmd interface {
	Type() cmdType
	Size() int
	Decode(b []byte) error
	Encode(b []byte) error
}

func newCmd(t cmdType) cmd {
	switch t {
	case cmdLogin:
		return &login{}
	case cmdJoin:
		return &join{}
	case cmdLeave:
		return &leave{}
	case cmdSend:
		return &send{}
	case cmdReceive:
		return &receive{}
	default:
		return nil
	}
}

// pkg ------------------------------------------------------------------------

type pkg struct {
	Type    cmdType
	Command cmd
}

func (p *pkg) decode(b []byte) (int, error) {
	n := len(b)
	if n < 4 {
		return 0, nil // less then minimum package size
	}

	size := int(stream.Uint16(b[0:2]))
	if size < 4 {
		return 0, fmt.Errorf("invalid cmd size %v < 4", size)
	}
	if n < size {
		return 0, nil // less then actual package size
	}

	p.Type = cmdType(b[3])
	n = size

	b = b[4:n]
	p.Command = newCmd(p.Type)
	if p.Command == nil {
		return n, fmt.Errorf("unknown cmdType %v", p.Type)
	}

	err := p.Command.Decode(b)
	if err != nil {
		return n, err
	}
	return n, nil
}

func (p *pkg) encode(b []byte) (int, error) {

	n := 4 + p.Command.Size()
	if m := len(b); m < n {
		return 0, fmt.Errorf("invalid buffer size (was %v bytes, expected at least %v bytes)", m, n)
	}

	stream.PutUint16(b[0:2], uint16(n))
	b[3] = byte(p.Type)
	err := p.Command.Encode(b[4:n])
	if err != nil {
		return 0, err
	}
	return n, nil
}

// ----------------------------------------------------------------------------

const (
	lgnMagic = 0x43110
	usrMin   = 4
	usrMax   = 0x7F
	chnMin   = 4
	chnMax   = 0x7F
	msgMin   = 1
	msgMax   = 0xFF
)

var stream = binary.LittleEndian

// login ----------------------------------------------------------------------

type login struct {
	User string
}

func (*login) Type() cmdType {
	return cmdLogin
}

func (login *login) Size() int {
	return 4 + len(login.User)
}

// Decode ...
func (login *login) Decode(b []byte) error {
	n := len(b)
	if k := 4 + usrMin; n < k {
		return fmt.Errorf("login.Decode: invalid buffer size (was %v bytes, expected at least %v bytes)", n, k)
	}
	if k := 4 + usrMax; n > k {
		return fmt.Errorf("login.Decode: invalid buffer size (was %v bytes, expected at most %v bytes)", n, k)
	}

	magic := stream.Uint32(b)
	if magic != lgnMagic {
		return fmt.Errorf("login.Decode: magic mismatch (was `%v`, expected `%v`)", magic, lgnMagic)
	}

	login.User = string(b[4:n])
	return nil
}

// Encode ...
func (login *login) Encode(b []byte) error {
	u := len(login.User)
	if u < usrMin || u > usrMax {
		return fmt.Errorf("login.Encode: invalid User (was %v bytes, expected between %v and %v", u, usrMin, usrMax)
	}
	n := 4 + u
	if k := len(b); k < n {
		return fmt.Errorf("login.Encode: invalid buffer size (was %v bytes, expected at least %v bytes)", k, n)
	}

	stream.PutUint32(b, lgnMagic)
	copy(b[4:4+u], []byte(login.User))
	return nil
}

// join -----------------------------------------------------------------------

type join struct {
	Channel string
}

func (*join) Type() cmdType {
	return cmdJoin
}

func (join *join) Size() int {
	return len(join.Channel)
}

// Decode ...
func (join *join) Decode(b []byte) error {
	n := len(b)
	if k := chnMin; n < k {
		return fmt.Errorf("join.Decode: invalid buffer size (was %v bytes, expected at least %v bytes)", n, k)
	}
	if k := chnMax; n > k {
		return fmt.Errorf("join.Decode: invalid buffer size (was %v bytes, expected at most %v bytes)", n, k)
	}

	join.Channel = string(b[0:n])
	return nil
}

// Encode ...
func (join *join) Encode(b []byte) error {
	n := len(join.Channel)
	if n < chnMin || n > chnMax {
		return fmt.Errorf("join.Encode: invalid channel (was %v bytes, expected between %v and %v", n, chnMin, chnMax)
	}
	if k := len(b); k < n {
		return fmt.Errorf("join.Encode: invalid buffer size (was %v bytes, expected at least %v bytes)", k, n)
	}

	copy(b[0:n], []byte(join.Channel))
	return nil
}

// leave ----------------------------------------------------------------------

type leave struct {
	Channel string
}

func (*leave) Type() cmdType {
	return cmdLeave
}

func (leave *leave) Size() int {
	return len(leave.Channel)
}

// Decode ...
func (leave *leave) Decode(b []byte) error {
	n := len(b)
	if k := chnMin; n < k {
		return fmt.Errorf("leave.Decode: invalid buffer size (was %v bytes, expected at least %v bytes)", n, k)
	}
	if k := chnMax; n > k {
		return fmt.Errorf("leave.Decode: invalid buffer size (was %v bytes, expected at most %v bytes)", n, k)
	}

	leave.Channel = string(b[0:n])
	return nil
}

// Encode ...
func (leave *leave) Encode(b []byte) error {
	n := len(leave.Channel)
	if n < chnMin || n > chnMax {
		return fmt.Errorf("leave.Encode: invalid channel (was %v bytes, expected between %v and %v", n, chnMin, chnMax)
	}

	if k := len(b); k < n {
		return fmt.Errorf("leave.Encode: invalid buffer size (was %v bytes, expected at least %v bytes)", k, n)
	}

	copy(b[0:n], []byte(leave.Channel))
	return nil
}

// send -----------------------------------------------------------------------

type send struct {
	Message string
	Channel string
}

func (*send) Type() cmdType {
	return cmdSend
}

func (send *send) Size() int {
	return 2 + len(send.Message) + len(send.Channel)
}

// Decode ...
func (send *send) Decode(b []byte) error {
	n := len(b)
	if k := 2 + msgMin + chnMin; n < k {
		return fmt.Errorf("send.Decode: invalid buffer size (was %v bytes, expected at least %v bytes)", n, k)
	}
	if k := 2 + msgMax + chnMax; n > k {
		return fmt.Errorf("send.Decode: invalid buffer size (was %v bytes, expected at most %v bytes)", n, k)
	}

	m := int(stream.Uint16(b))
	if m < msgMin || m > msgMax {
		return fmt.Errorf("send.Decode: invalid message length (was %v bytes, expected between %v and %v bytes)", m, msgMin, msgMax)
	}
	c := n - (2 + m)
	if c < chnMin || c > chnMax {
		return fmt.Errorf("send.Decode: invalid channel length (was %v bytes, expected between %v and %v bytes)", c, chnMin, chnMax)
	}

	send.Message = string(b[2 : 2+m])
	send.Channel = string(b[2+m : n])
	return nil
}

// Encode ...
func (send *send) Encode(b []byte) error {
	m := len(send.Message)
	if m < msgMin || m > msgMax {
		return fmt.Errorf("leave.Encode: invalid message (was %v bytes, expected between %v and %v", m, msgMin, msgMax)
	}
	c := len(send.Channel)
	if c < chnMin || c > chnMax {
		return fmt.Errorf("leave.Encode: invalid channel (was %v bytes, expected between %v and %v", c, chnMin, chnMax)
	}

	n := 2 + c + m
	if k := len(b); k < n {
		return fmt.Errorf("send.Encode: invalid buffer size (was %v bytes, expected at least %v bytes)", k, n)
	}

	stream.PutUint16(b, uint16(m))
	copy(b[2:2+m], []byte(send.Message))
	copy(b[2+m:n], []byte(send.Channel))
	return nil
}

// receive --------------------------------------------------------------------

type receive struct {
	Time    time.Time
	Message string
	User    string
	Channel string
}

func (*receive) Type() cmdType {
	return cmdReceive
}

func (receive *receive) Size() int {
	return 12 + len(receive.Message) + len(receive.User) + len(receive.Channel)
}

// Decode ...
func (receive *receive) Decode(b []byte) error {
	n := len(b)
	if k := 12 + msgMin + usrMin + chnMin; n < k {
		return fmt.Errorf("receive.Decode: invalid buffer size (was %v bytes, expected at least %v bytes)", n, k)
	}
	if k := 12 + msgMax + usrMax + chnMax; n > k {
		return fmt.Errorf("receive.Decode: invalid buffer size (was %v bytes, expected at most %v bytes)", n, k)
	}

	m := int(stream.Uint16(b[0:2]))
	if m < msgMin || m > msgMax {
		return fmt.Errorf("receive.Decode: invalid message length (was %v bytes, expected between %v and %v bytes)", m, msgMin, msgMax)
	}
	u := int(stream.Uint16(b[2:4]))
	if u < usrMin || u > usrMax {
		return fmt.Errorf("receive.Decode: invalid user length (was %v bytes, expected between %v and %v bytes)", u, usrMin, usrMax)
	}
	c := n - (12 + u + m)
	if c < chnMin || c > chnMax {
		return fmt.Errorf("receive.Decode: invalid channel length (was %v bytes, expected between %v and %v bytes)", c, chnMin, chnMax)
	}

	tSec := stream.Uint32(b[4:8])
	tNano := stream.Uint32(b[8:12])
	receive.Time = time.Unix(int64(tSec), int64(tNano))

	receive.Message = string(b[12 : 12+m])
	receive.User = string(b[12+m : 12+m+u])
	receive.Channel = string(b[12+m+u : n])
	return nil
}

// Encode ...
func (receive *receive) Encode(b []byte) error {
	m := len(receive.Message)
	if m < msgMin || m > msgMax {
		return fmt.Errorf("receive.Encode: invalid message (was %v bytes, expected between %v and %v", m, msgMin, msgMax)
	}
	u := len(receive.User)
	if u < usrMin || u > usrMax {
		return fmt.Errorf("receive.Encode: invalid user (was %v bytes, expected between %v and %v", u, usrMin, usrMax)
	}
	c := len(receive.Channel)
	if c < chnMin || c > chnMax {
		return fmt.Errorf("receive.Encode: invalid channel (was %v bytes, expected between %v and %v", c, chnMin, chnMax)
	}

	n := 12 + m + u + c
	if k := len(b); k < n {
		return fmt.Errorf("receive.Encode: invalid buffer size (was %v bytes, expected at least %v bytes)", k, n)
	}

	tSec := receive.Time.Unix()
	tNano := receive.Time.UnixNano()

	stream.PutUint16(b[0:2], uint16(m))
	stream.PutUint16(b[2:4], uint16(u))
	stream.PutUint32(b[4:8], uint32(tSec))
	stream.PutUint32(b[8:12], uint32(tNano))
	copy(b[12:12+m], []byte(receive.Message))
	copy(b[12+m:12+m+u], []byte(receive.User))
	copy(b[12+m+u:n], []byte(receive.Channel))
	return nil
}
