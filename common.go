package chat

import (
	"fmt"
	"io"
	"time"
)

// Message ...
type Message struct {
	Time    time.Time
	Sender  string
	Channel string
	Content string
}

// IO represents a generic network API
type IO interface {

	// Refresh performs write & read operations on a socket. Accepting connections in case if a server.
	Refresh() error

	// Close terminates the socket connection and releases buffers.
	Close() error
}

// ----------------------------------------------------------------------------

func receiveLoop(reader io.Reader, out chan<- *pkg) error {
	buffer := make([]byte, 0xFFFF)
	read := 0
	for {
		n, err := reader.Read(buffer[read:])
		if err != nil {
			return fmt.Errorf("chat.receiveLoop reading failed %w", err)
		}

		read += n
		done := 0

		for {
			pkg := pkg{}
			n, err = pkg.decode(buffer[done:read])
			if err != nil {
				return fmt.Errorf("chat.receiveLoop decoding failed: %w", err)
			}
			if n <= 0 {
				break
			}

			done += n
			out <- &pkg
		}

		read = copy(buffer, buffer[done:read])
	}
}

func sendLoop(writer io.Writer, in <-chan *pkg) error {
	buffer := make([]byte, 0xFFFF)
	for {
		pkg := <-in
		if pkg == nil {
			return nil // NOTE(micha): channel was closed
		}
		n, err := pkg.encode(buffer)
		if err != nil {
			return fmt.Errorf("chat.sendLoop encoding failed: %w", err)
		}
		b := buffer[:n]
		for len(b) > 0 {
			n, err = writer.Write(b)
			if err != nil {
				return fmt.Errorf("chat.sendLoop writing failed %w", err)
			}
			b = b[n:]
		}
	}
}

// ----------------------------------------------------------------------------

type clientEvents interface {
	onCmdJoin(channel string)
	onCmdLeave(channel string)
	onCmdReceive(message *Message)
}

func handleClientEvents(evt clientEvents, p *pkg) bool {
	switch p.Type {
	case cmdJoin:
		join := p.Command.(*join)
		evt.onCmdJoin(join.Channel)
	case cmdLeave:
		leave := p.Command.(*leave)
		evt.onCmdLeave(leave.Channel)
	case cmdReceive:
		receive := p.Command.(*receive)
		evt.onCmdReceive(&Message{
			Time:    receive.Time,
			Sender:  receive.User,
			Channel: receive.Channel,
			Content: receive.Message,
		})
	default:
		return false
	}
	return true
}

// ----------------------------------------------------------------------------

type peerEvents interface {
	onCmdLogin(user string)
	onCmdJoin(channel string)
	onCmdLeave(channel string)
	onCmdSend(channel, message string)
}

func handlePeerEvents(evt peerEvents, p *pkg) bool {
	switch p.Type {
	case cmdLogin:
		login := p.Command.(*login)
		evt.onCmdLogin(login.User)
	case cmdJoin:
		join := p.Command.(*join)
		evt.onCmdJoin(join.Channel)
	case cmdLeave:
		leave := p.Command.(*leave)
		evt.onCmdJoin(leave.Channel)
	case cmdSend:
		send := p.Command.(*send)
		evt.onCmdSend(send.Channel, send.Message)
	default:
		return false
	}
	return true
}
