package chat

import (
	"container/list"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"time"
)

// Server API to currently the user and channel counts.
type Server interface {

	// Users returns connected user count
	Users() int

	// Channels returns connected active count with at least a single connected user.
	Channels() int
}

// ServerIO combines the server and network APIs.
type ServerIO interface {
	Server
	IO
}

// NewServer launches a chat server and binds it to the address.
// The format is hostname:port
func NewServer(addr string, logger *log.Logger) (ServerIO, error) {
	if logger == nil {
		logger = log.New(io.Discard, "", 0)
	}
	return newServerIO(addr, 0, logger)
}

// ----------------------------------------------------------------------------

const (
	// total user limit
	usrLmt = 4096
	// total channel limit
	chnLmt = 4096
	// limits users per channel
	chnUsrLmt = 4096
	// limits channels per user
	usrChnLmt = 128
)

// ----------------------------------------------------------------------------

type serverIO struct {
	listener   *net.TCPListener
	connect    chan *net.TCPConn
	disconnect chan *peerIO
	buffer     int
	peers      []*peerIO
	server
}

func newServerIO(addr string, buffer int, log *log.Logger) (*serverIO, error) {
	tcpAddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, fmt.Errorf("newServerIO: resolving TCP address failed %w", err)
	}
	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		return nil, fmt.Errorf("newServerIO: binding TCP address failed %w", err)
	}

	io := serverIO{
		listener:   listener,
		connect:    make(chan *net.TCPConn),
		disconnect: make(chan *peerIO),
		buffer:     buffer,
		peers:      make([]*peerIO, 0, 0xFFFF), // just to prevent constant growing
		server: server{
			users:    make(map[string]struct{}),
			channels: make(map[string]*room),
			log:      log,
		},
	}

	go io.accept()

	return &io, nil
}

func (s *serverIO) accept() error { // go-routine
	defer s.Close()
	for {
		conn, err := s.listener.AcceptTCP()
		if err != nil {
			log.Printf("AcceptTCP failed: %v", err)
			return err
		}
		s.connect <- conn
	}
}

func (s *serverIO) Close() error {
	close(s.connect)
	return s.listener.Close()
}

func (s *serverIO) Refresh() error {
	select {
	case peer := <-s.disconnect:
		if peer != nil {
			peer.Close()
		}
	default:
		break
	}

	select {
	case conn := <-s.connect:
		if conn == nil {
			break
		}
		if n := len(s.users); n >= usrLmt {
			conn.Close()
			log.Printf("total user limit reached: %v", usrLmt)
			break
		}
		s.newPeer(conn, s.buffer)
	default:
		break
	}

	for _, p := range s.peers {
		err := p.Refresh()
		if err != nil {
			log.Printf("peer[%v] error: %v", p.user, err)
			p.Close()
			return err
		}
	}
	return nil
}

func (s *serverIO) newPeer(conn *net.TCPConn, buffer int) {
	io := peerIO{
		conn:   conn,
		in:     make(chan *pkg, buffer),
		out:    make(chan *pkg, buffer),
		index:  len(s.peers),
		server: s,
		peer: peer{
			cmds:   list.New(),
			server: &s.server,
		},
	}
	s.peers = append(s.peers, &io)

	go io.receive()
	go io.send()
}

type peerIO struct {
	conn   *net.TCPConn
	in     chan *pkg
	out    chan *pkg
	index  int
	server *serverIO
	peer
}

func (p *peerIO) receive() error { // go-routine
	defer func() {
		p.server.disconnect <- p
	}()
	return receiveLoop(p.conn, p.in)
}

func (p *peerIO) send() error { // go-routine
	defer func() {
		p.server.disconnect <- p
	}()
	return sendLoop(p.conn, p.out)
}

func (p *peerIO) Close() error {
	if p.index < 0 {
		return nil
	}
	index := p.index
	p.index = -index - 1

	p.peer.Disconnect()
	err := p.conn.Close()

	// remove peer from list
	n := len(p.server.peers) - 1
	if n > 0 && n != index {
		last := p.server.peers[n]
		last.index = index
		p.server.peers[index] = last
	}
	p.server.peers[n] = nil
	p.server.peers = p.server.peers[:n]

	close(p.out)
	return err
}

func (p *peerIO) Refresh() error {
	if p.cmds.Len() > 0 {
		elem := p.cmds.Front()
		pkg := elem.Value.(*pkg)
		select {
		case p.out <- pkg:
			p.cmds.Remove(elem)
		default:
			break
		}
	}

	select {
	case pkg := <-p.in:
		ok := handlePeerEvents(&p.peer, pkg)
		if !ok {
			log.Printf("server peer[%v] event unhandled: %v", p.user, pkg.Type)
		}
	default:
		break
	}

	return nil
}

// ----------------------------------------------------------------------------

type room struct {
	id    string
	peers map[string]*peer
}

type server struct {
	users    map[string]struct{}
	channels map[string]*room
	log      *log.Logger
}

func (s *server) Users() int {
	return len(s.users)
}

func (s *server) Channels() int {
	return len(s.channels)
}

type peer struct {
	user     string
	channels []string
	server   *server
	cmds     *list.List
}

func (p *peer) onCmdLogin(user string) {
	if p.user != "" {
		log.Printf("serverPeer.onCmdLogin(%v): already logged in as '%v'", user, p.user)
		return
	}

	id := user
	clone := 1
	for {
		_, exists := p.server.users[id]
		if !exists {
			p.server.users[id] = struct{}{}
			break
		}

		clone++
		id = user + "#" + strconv.Itoa(clone)
	}

	p.user = id
	p.server.log.Printf("login: %v >", p.user)
}

func (p *peer) onCmdJoin(channel string) {
	if p.user == "" {
		log.Printf("serverPeer.onCmdJoin(%v): not logged in", channel)
		return // not logged in
	}

	if n := len(p.server.channels); n >= chnLmt {
		_, exists := p.server.channels[channel]
		if !exists {
			log.Printf("serverPeer.onCmdJoin(%v): total channel limit reached %v (%v)", channel, chnLmt, p.user)
			return
		}
	}

	{
		joined := false
		for _, chn := range p.channels {
			joined = chn == channel
			if joined {
				break
			}
		}
		if joined {
			log.Printf("serverPeer.onCmdJoin(%v): already joined (%v)", channel, p.user)
			return
		}
	}

	if n := len(p.channels); n >= usrChnLmt {
		log.Printf("serverPeer.onCmdJoin(%v): channels per user limit reached %v (%v)", channel, usrChnLmt, p.user)
		return
	}

	users := 0
	chn := p.server.channels[channel]
	if chn != nil {
		users = len(chn.peers)
	}
	if users >= chnUsrLmt {
		log.Printf("serverPeer.onCmdJoin(%v): user per channel limit reached %v (%v)", channel, chnUsrLmt, p.user)
		return
	}

	if chn == nil {
		chn = &room{
			id:    channel,
			peers: make(map[string]*peer),
		}
		p.server.channels[channel] = chn
	}

	chn.peers[p.user] = p
	p.channels = append(p.channels, channel)

	p.cmds.PushBack(&pkg{
		Type: cmdJoin,
		Command: &join{
			Channel: channel,
		},
	})

	p.server.log.Printf("[%v] %v joined", channel, p.user)
}

func (p *peer) onCmdLeave(channel string) {
	if p.user == "" {
		log.Printf("serverPeer.onCmdLeave(%v): not logged in", channel)
		return // not logged in
	}

	{
		invalid := true
		for i, chn := range p.channels {
			if chn != channel {
				continue
			}
			n := len(p.channels) - 1
			p.channels[i] = p.channels[n]
			p.channels = p.channels[:n]
			invalid = false
			break
		}
		if invalid {
			log.Printf("serverPeer.onCmdLeave(%v): user wasn't in the the channel (%v)", channel, p.user)
			return
		}
	}

	chn := p.server.channels[channel]
	if chn == nil {
		log.Printf("serverPeer.onCmdLeave(%v): channel doesn't exist (%v)", channel, p.user)
		return
	}
	delete(chn.peers, p.user)
	if len(chn.peers) == 0 {
		delete(p.server.channels, channel)
	}

	p.cmds.PushBack(&pkg{
		Type: cmdLeave,
		Command: &leave{
			Channel: channel,
		},
	})

	p.server.log.Printf("[%v] %v left", channel, p.user)
}

func (p *peer) onCmdSend(channel string, message string) {
	if p.user == "" {
		log.Printf("serverPeer.onCmdSend(%v): not logged in", channel)
		return // not logged in
	}

	{
		joined := false
		for _, chn := range p.channels {
			joined = chn == channel
			if joined {
				break
			}
		}
		if !joined {
			p.onCmdJoin(channel) // try join implicitly
		}
	}

	chn := p.server.channels[channel]
	if chn == nil {
		log.Printf("serverPeer.onCmdSend(%v): channel does not exist (%v)", channel, p.user)
		return
	}

	{
		joined := false
		for _, chn := range p.channels {
			joined = chn == channel
			if joined {
				break
			}
		}
		if !joined {
			log.Printf("serverPeer.onCmdSend(%v): user isn't in the channel (%v)", channel, p.user)
		}
	}

	pkg := pkg{
		Type: cmdReceive,
		Command: &receive{
			Time:    time.Now().UTC(),
			Message: message,
			User:    p.user,
			Channel: channel,
		},
	}
	for _, peer := range chn.peers {
		peer.cmds.PushBack(&pkg)
	}
	p.server.log.Printf("[%v] %v > %v", channel, p.user, message)
}

func (p *peer) Disconnect() {
	for _, channel := range p.channels {
		chn := p.server.channels[channel]
		if chn == nil {
			continue
		}
		delete(chn.peers, p.user)
		if len(chn.peers) == 0 {
			delete(p.server.channels, chn.id)
		}
	}
	delete(p.server.users, p.user)
}

// ----------------------------------------------------------------------------
