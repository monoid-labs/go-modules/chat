# TOOLS -----------------------------------------------------------------------
GO = go

# PAHTS -----------------------------------------------------------------------
BIN = bin

# GOALS -----------------------------------------------------------------------
build:
	$(GO) build -race -trimpath -o $(BIN)/ ./cmd/...

clean:
	rm -rf $(BIN)

.PHONY: clean build # run without files changed

