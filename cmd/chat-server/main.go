package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"monoid.net/chat"
)

func main() {
	var addr = ":7788"

	flag.Parse()
	if flag.NArg() > 0 {
		addr = flag.Arg((0))
	}

	// log files
	chatLog, err := os.OpenFile("chat.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatalf("could not open/create chat.log file: %v\n", err)
		return
	}
	defer chatLog.Close()

	chatStats, err := os.OpenFile("chat.stats", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatalf("could not open/create chat.stats file: %v\n", err)
		return
	}
	defer chatStats.Close()
	// delete stats file on exit
	go func() {
		exit := make(chan os.Signal, 1)
		signal.Notify(exit, syscall.SIGINT, syscall.SIGTERM)
		<-exit
		os.Remove("chat.stats")
		os.Exit(0)
	}()

	chatLogger := log.New(chatLog, "", log.LUTC|log.Ldate|log.Ltime)
	statsLogger := log.New(chatStats, "", log.LUTC|log.Ldate|log.Ltime)

	// chat service
	server, err := chat.NewServer(addr, chatLogger)
	if err != nil {
		log.Fatalf("chat.NewServer() failed: %v\n", err)
		return
	}
	defer server.Close()

	// loop
	const wait = time.Minute
	last := time.Now().Add(-wait)

	for {
		// network io (accept, send & receive)
		err := server.Refresh()
		if err != nil {
			break
		}

		// if interval has passed
		now := time.Now()
		if passed := now.Sub(last); passed < wait {
			continue
		}
		last = now

		// print stats
		chatStats.Truncate(0)
		statsLogger.Printf("Users: %v | Channels: %v | Go-Routines: %v\n", server.Users(), server.Channels(), runtime.NumGoroutine())
		// runtime.GC()
	}

}
