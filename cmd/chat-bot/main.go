package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"

	"monoid.net/chat"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	addr := ""

	flag.Parse()
	if flag.NArg() > 0 {
		addr = flag.Arg(0)
	}
	if addr == "" {
		fmt.Println("Usage: chat-client username@hostname:port")
		return
	}

	client, err := chat.NewClient(addr)
	if err != nil {
		log.Fatalf("chat.NewClient() failed: %v\n", err)
		return
	}
	defer client.Close()

	app := chatApp{
		client: client,
	}

	client.Join("global")

	if rand.Intn(2)%2 == 0 {
		client.Join("random-0")
	} else {
		client.Join("random-1")
	}

	messages := 16 + rand.Intn(16)
	msg := 0
	wait := time.Second
	last := time.Now()

	for {
		// network
		if err := app.RefreshNetwork(); err != nil {
			log.Fatalf("%v\n", err)
			return
		}
		// application
		app.ReceiveMessages()

		now := time.Now()
		if now.Sub(last) < wait {
			continue
		}
		last = now

		if msg == messages {
			break
		}

		app.SendMessage("message n." + strconv.Itoa(msg))
		msg++
		wait = time.Millisecond * time.Duration(rand.Intn(1000))
	}

	fmt.Printf("OVER & OUT\n")
}

type chatApp struct {
	client   chat.ClientIO
	channels [8]string
	messages [32]chat.Message
}

func (app *chatApp) RefreshNetwork() error {
	if err := app.client.Refresh(); err != nil {
		return fmt.Errorf("chat.ClientIO.Refresh() failed: %w", err)
	}
	return nil
}

func (app *chatApp) SendMessage(message string) {
	n := app.client.Channels(app.channels[:])
	if n == 0 {
		return
	}
	i := rand.Intn(n)
	app.client.Send(app.channels[i], message)
}

func (app *chatApp) ReceiveMessages() {
	for {
		n := app.client.Receive(app.messages[:])
		for _, msg := range app.messages[:n] {
			fmt.Printf("Message[%v] %v > %v\n", msg.Channel, msg.Sender, msg.Content)
		}
		if n <= 0 {
			break
		}
	}
}
