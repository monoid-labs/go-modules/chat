package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"

	"monoid.net/chat"
)

func main() {
	addr := ""

	flag.Parse()
	if flag.NArg() > 0 {
		addr = flag.Arg(0)
	}
	if addr == "" {
		fmt.Println("Usage: chat-client username@hostname:port")
		return
	}

	client, err := chat.NewClient(addr)
	if err != nil {
		log.Fatalf("chat.NewClient() failed: %v\n", err)
		return
	}
	defer client.Close()

	var messages [32]chat.Message
	inputs := make(chan string)

	go func() {
		for {
			in := bufio.NewReader(os.Stdin)
			line, err := in.ReadString('\n')
			if err == nil {
				inputs <- line
				continue
			}
			fmt.Println(err)
			close(inputs)
			return
		}
	}()

	client.Join("global")

	for {
		if err := client.Refresh(); err != nil {
			log.Fatalf("chat.Client.Refresh() %v\n", err)
			return
		}

		// receive
		for {
			n := client.Receive(messages[:])
			if n <= 0 {
				break
			}
			for _, msg := range messages[:n] {
				fmt.Printf("Msg: [%v] %v > %v\n", msg.Channel, msg.Sender, msg.Content)
			}
		}

		input := ""

		select {
		case line, ok := <-inputs:
			if !ok {
				return
			}
			input = line
		default: // -> non-blocking
		}

		if input == "" {
			continue
		}

		client.Send("global", input)
	}

}
