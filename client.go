package chat

import (
	"container/list"
	"fmt"
	"log"
	"net"
	"strings"
)

// Client API to join/leave channels and send/receive messages.
type Client interface {
	// Join adds the user to a channel.
	Join(channel string)

	// Leave removes the user from a (previously joined) channel.
	Leave(channel string)

	// Sends a message to a channel
	Send(channel string, message string)

	// Receive teturns the unread messages the client received.
	// If `messages` is `nil` the total count is returned.
	// Otherwise as many as fit are copied the amount is returned.
	// Once copied the messages are removed from the client.
	Receive(messages []Message) int

	// Channels returns the number of channels the client is part of.
	// If `channels` is `nil` the total count is returned.
	// Otherwise as many as fit are copied the amount is returned.
	Channels(channels []string) int
}

// ClientIO combines the client and network APIs.
type ClientIO interface {
	Client
	IO
}

// NewClient launches a chat client and connects it to the address.
// The format is username@hostname:port
func NewClient(addr string) (ClientIO, error) {
	split := strings.Index(addr, "@")
	if split < 0 {
		return nil, fmt.Errorf("chat.NewClient expects an address in the form of 'username@hostname:port' but was '%v'", addr)
	}
	usr := addr[:split]
	addr = addr[split+1:]
	return newClientIO(addr, usr, 0)
}

// ----------------------------------------------------------------------------

type clientIO struct {
	conn *net.TCPConn
	in   chan *pkg
	out  chan *pkg
	client
}

func newClientIO(addr string, usr string, buffer int) (*clientIO, error) {
	tcpAddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, fmt.Errorf("newClientIO: resolving TCP address failed %w", err)
	}
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return nil, fmt.Errorf("newClientIO: dailing TCP address failed %w", err)
	}

	io := clientIO{
		conn: conn,
		in:   make(chan *pkg, buffer),
		out:  make(chan *pkg, buffer),
		client: client{
			channels: make([]string, 0, 8),
			messages: make([]Message, 0, 32),
			cmds:     list.New(),
		},
	}

	go receiveLoop(io.conn, io.in)
	go sendLoop(io.conn, io.out)

	io.client.cmds.PushBack(&pkg{
		Type:    cmdLogin,
		Command: &login{User: usr},
	})

	return &io, nil
}

func (c *clientIO) Close() error {
	if c.conn == nil {
		return nil
	}

	err := c.conn.Close()
	c.conn = nil
	return err
}

func (c *clientIO) Refresh() error {
	for {
		if c.client.cmds.Len() > 0 {
			elem := c.client.cmds.Front()
			pkg := elem.Value.(*pkg)
			select {
			case c.out <- pkg:
				c.client.cmds.Remove(elem)
			default:
				break
			}
		}

		select {
		case pkg := <-c.in:
			ok := handleClientEvents(&c.client, pkg)
			if !ok {
				log.Printf("unhandled client event '%v'", pkg.Type)
			}
		default:
			return nil
		}
	}
}

// ----------------------------------------------------------------------------

type client struct {
	channels []string
	messages []Message
	cmds     *list.List
}

func (c *client) onCmdJoin(channel string) {
	for _, name := range c.channels {
		if name == channel {
			log.Printf("user joining was already in channel '%v'", channel)
			return // already joined
		}
	}
	c.channels = append(c.channels, channel)
}

func (c *client) onCmdLeave(channel string) {
	for i, name := range c.channels {
		if name != channel {
			continue
		}
		n := len(c.channels) - 1
		c.channels[i] = c.channels[n]
		c.channels[n] = ""
		c.channels = c.channels[:n]
	}
	log.Printf("user leaving was not in channel '%v'", channel)
}

func (c *client) onCmdReceive(message *Message) {
	c.messages = append(c.messages, *message)
}

// ----------------------------------------------------------------------------

func (c *client) Channels(channels []string) int {
	if channels == nil {
		return len(c.channels)
	}
	return copy(channels, c.channels)
}

func (c *client) Receive(messages []Message) int {
	n := len(c.messages)
	if messages == nil {
		return n
	}

	if m := copy(messages, c.messages); m > 0 {
		k := copy(c.messages, c.messages[m:])
		c.messages = c.messages[:k]
		n = m
	}

	return n
}

func (c *client) Join(channel string) {
	c.cmds.PushBack(&pkg{
		Type:    cmdJoin,
		Command: &join{Channel: channel},
	})
}

func (c *client) Leave(channel string) {
	c.cmds.PushBack(&pkg{
		Type:    cmdLeave,
		Command: &leave{Channel: channel},
	})
}

func (c *client) Send(channel string, message string) {
	c.cmds.PushBack(&pkg{
		Type:    cmdSend,
		Command: &send{Message: message, Channel: channel},
	})
}
